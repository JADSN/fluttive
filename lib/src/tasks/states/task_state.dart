import 'package:learningvaluenotifier/src/tasks/models/task_model.dart';

abstract class TaskState {}

class InitialTaskState extends TaskState {}

class SuccessTaskState extends TaskState {
  final List<TaskModel> tasks;

  SuccessTaskState(this.tasks);
}

class LoadingTaskState extends TaskState {}

class ErrorTaskState extends TaskState {
  final String message;

  ErrorTaskState(this.message);
}
