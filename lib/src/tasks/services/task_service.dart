import 'dart:io';

import 'package:uno/uno.dart';

import 'package:learningvaluenotifier/src/tasks/models/task_model.dart';

class TaskService {
  final Uno uno;

  TaskService(this.uno);

  Future<List<TaskModel>> fetchTasks() async {
    final String host = Platform.isAndroid ? "10.0.2.2" : "localhost";

    final response = await uno.get(
      "http://$host:3000/tasks",
      headers: {
        'Content-Type': 'application/json',
      },
    );
    final list = response.data as List;
    final result = list.map((map) => TaskModel.fromMap(map)).toList();
    return result;
  }
}
