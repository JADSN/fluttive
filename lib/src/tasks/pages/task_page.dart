import 'package:flutter/material.dart';
import 'package:learningvaluenotifier/src/tasks/models/task_model.dart';

import 'package:learningvaluenotifier/src/tasks/states/task_state.dart';
import 'package:learningvaluenotifier/src/tasks/stores/task_store.dart';
import 'package:provider/provider.dart';

class TaskPage extends StatefulWidget {
  const TaskPage({Key? key}) : super(key: key);

  @override
  State<TaskPage> createState() => _TaskPageState();
}

class _TaskPageState extends State<TaskPage> {
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      context.read<TaskStore>().fetchTasks();
    });
  }

  @override
  Widget build(BuildContext context) {
    final store = context.watch<TaskStore>();

    final state = store.value;

    Widget? resultWidget;

    if (state is LoadingTaskState) {
      resultWidget = const Center(
        child: CircularProgressIndicator(),
      );
    }

    if (state is ErrorTaskState) {
      resultWidget = Center(
        child: Text(state.message),
      );
    }

    if (state is SuccessTaskState) {
      resultWidget = ListView.builder(
        itemCount: state.tasks.length,
        itemBuilder: (context, index) {
          final TaskModel task = state.tasks.elementAt(index);
          return ListTile(
            title: Text(task.description),
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Fluttive'),
      ),
      body: resultWidget ?? Container(),
    );
  }
}
