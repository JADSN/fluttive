import 'package:flutter/material.dart';

import 'package:learningvaluenotifier/src/tasks/services/task_service.dart';
import 'package:learningvaluenotifier/src/tasks/states/task_state.dart';

class TaskStore extends ValueNotifier<TaskState> {
  final TaskService taskService;

  TaskStore(
    this.taskService,
  ) : super(InitialTaskState());

  Future<void> fetchTasks() async {
    value = LoadingTaskState();
    await Future.delayed(const Duration(seconds: 3));
    try {
      final tasks = await taskService.fetchTasks();
      value = SuccessTaskState(tasks);
    } on Exception catch (e) {
      value = ErrorTaskState(e.toString());
    }
  }
}
