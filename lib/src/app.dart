import 'package:flutter/material.dart';

import 'package:learningvaluenotifier/src/tasks/pages/task_page.dart';
import 'package:learningvaluenotifier/src/tasks/services/task_service.dart';
import 'package:learningvaluenotifier/src/tasks/stores/task_store.dart';
import 'package:provider/provider.dart';
import 'package:uno/uno.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          create: (_) => Uno(),
        ),
        Provider(
          create: (context) => TaskService(context.read()),
        ),
        ChangeNotifierProvider(
          create: (context) => TaskStore(context.read()),
        )
      ],
      child: MaterialApp(
        title: 'Fluttive',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const TaskPage(),
      ),
    );
  }
}
