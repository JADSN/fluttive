import 'package:flutter/material.dart';

// class HomeController {
//   final ValueNotifier<int> counter$ = ValueNotifier(0);

//   int get counter => counter$.value;

//   void increment() => counter$.value++;
// }

class CounterController extends ValueNotifier<int> {
  CounterController() : super(0);

  void increment() => value++;
  void decrement() => value--;
}
