import 'package:flutter/material.dart';
import 'package:learningvaluenotifier/src/home/home_controller.dart';

class HomePage extends StatefulWidget {
  const HomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var counter = CounterController();

  // @override
  // void initState() {
  //   super.initState();
  //   counter.addListener(() => setState(() {}));
  // }

  @override
  Widget build(BuildContext context) {
    debugPrint("BUILD");
    return Scaffold(
      appBar: AppBar(
        title: const Text("Stater"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            // Text(
            //   '${counter.value}',
            //   style: Theme.of(context).textTheme.headline4,
            // ),
            ValueListenableBuilder<int>(
              valueListenable: counter,
              builder: (_, value, child) {
                return Text(
                  '$value',
                  style: Theme.of(context).textTheme.headline4,
                );
              },
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: counter.increment,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
